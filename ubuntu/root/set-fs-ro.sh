#!/bin/bash
# if this system is perhaps running in read write mode. Set to read only mode on next reboot and also reboot

flagreboot=0
if test -e /etc/overlayroot.local.conf_disable ; then
    mv /etc/overlayroot.local.conf_disable /etc/overlayroot.local.conf || true
    flagreboot=1
fi
if test -e /media/root-ro/etc/overlayroot.local.conf_disable ; then
    mount -o remount,rw /media/root-ro || true
    mv /media/root-ro/etc/overlayroot.local.conf_disable /media/root-ro/etc/overlayroot.local.conf || true
    flagreboot=1
fi

if test "$flagreboot" = '1' ; then
    reboot
fi
