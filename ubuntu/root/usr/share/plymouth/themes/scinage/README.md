changes to the boot screen requires updating the initrd

- kernel boots with initrd.
- booting hits script /usr/share/initramfs-tools/scripts/init-premount/plymouth

apt install -y plymouth-themes

ln -sf /usr/share/plymouth/themes/scinage/scinage.plymouth /etc/alternatives/default.plymouth
ln -sf /usr/share/plymouth/themes/bgrt/bgrt.plymouth /etc/alternatives/default.plymouth


<!--
mkdir /etc/plymouth
<<EOF cat > /etc/plymouth/plymouthd.conf
[Daemon]
Theme=myplymouththeme
ShowDelay=0
EOF-->


update-initramfs -u
update-grub



# make sure you have the packages for plymouth
sudo apt install plymouth

# after downloading or cloning themes, copy the selected theme in plymouth theme dir
sudo cp -r angular /usr/share/plymouth/themes/

# install the new theme (angular, in this case)
sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/angular/angular.plymouth 100

# select the theme to apply
sudo update-alternatives --config default.plymouth
#(select the number for installed theme, angular in this case)

# update initramfs
sudo update-initramfs -u







# update-alternatives: using /usr/share/plymouth/themes/bgrt/bgrt.plymouth to provide /usr/share/plymouth/themes/default.plymouth (default.plymouth) in auto mode





cp /usr/share/plymouth/themes/scinage/scinage.plymouth /lib/plymouth/themes/default.plymouth


/etc/alternatives/default.plymouth
/usr/share/plymouth/themes/default.plymouth

# /lib/plymouth/themes

cp --remove-destination /usr/share/plymouth/themes/default.plymouth  /etc/alternatives/default.plymouth

```
ln -sf /usr/share/plymouth/themes/scinage/scinage.plymouth /etc/alternatives/default.plymouth

# ubuntu can do that with
update-initramfs -u
update-grub
```


sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/scinage/scinage.plymouth 100

update-alternatives --config default.plymouth
