#!/bin/bash
# programs source this file for env vars.
SERVER='scinage.uwaterloo.ca'
SITE_URL="https://${SERVER}"
DEVICE_ID=$(cat /sys/class/dmi/id/board_serial)_$(cat /sys/class/dmi/id/product_uuid | true)
MODEL_ID=$(cat /proc/cpuinfo | grep -E '[mM]odel.*:' | head -n1 | cut -d ':' -f2 | tr -d ' ')
if test -e /sys/class/dmi/id/product_name ; then
MODEL_ID="$(cat /sys/class/dmi/id/product_name)_$MODEL_ID"
fi
#MODEL_ID=$(cat /proc/cpuinfo | grep 'model name.*:' | head -n1 | cut -d ':' -f2 | tr -d ' ')
SCREEN_ID="sw-$(echo $DEVICE_ID | sha256sum | cut -c1-7)"
SCREEN_URL="${SITE_URL}/screens/view/${SCREEN_ID}/"
SCREEN_SETUP_URL="${SITE_URL}/screens/new/?id_title=${SCREEN_ID}"
WS_REFRESH_RATE=10
WS_DEBUG_TIMEOUT=4
WS_HTML_TPL=/srv/kiosk-ws.html
NETWORK_TEST_IPV4=129.97.2.1
NETWORK_TEST_SERVER_1=scinage.uwaterloo.ca
NETWORK_TEST_SERVER_2=uwaterloo.ca
NETWORK_TEST_INTERNET=google.ca
WHEN_NEEDED_HTTP_PROXY=calamar.private.uwaterloo.ca:3128
TIMEZONE=America/Toronto

CHECK_UPDATES_ENDPOINT=https://bringup.fast.uwaterloo.ca/org/scinage/dev/$SCREEN_ID/ep/scinage
SEND_METRICS_ENDPOINT=https://bringup.fast.uwaterloo.ca/org/scinage/dev/$SCREEN_ID/set/$(date -I)
