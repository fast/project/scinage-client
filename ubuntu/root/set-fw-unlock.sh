#!/bin/bash
unlink /var/lock/fwlock || true

# set some pi leds
echo heartbeat > /sys/class/leds/led0/trigger || true
echo 1 > /sys/class/leds/led0/brightness || true
echo 0 > /sys/class/leds/led1/brightness || true
