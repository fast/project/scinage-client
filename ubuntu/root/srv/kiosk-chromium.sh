#!/bin/bash
xset s noblank
xset s off
xset -dpms
source /config.sh
source /config-screens.sh

browser_bin=chromium-browser
browser_video_switches='--enable-zero-copy --force-gpu-rasterization --canvas-oop-rasterization'

## apparrmor really does not like running chrome as root :)
## so lets bypass running the browser contained as a snap
if command -v /usr/bin/aa-enabled ; then
if test -e /snap/chromium/current/usr/lib/chromium-browser/chrome ; then
# aa-status lists that apparmor enforce on snap.chromium.chromium...
#systemctl stop apparmor.service || true
#rm -rf /var/lib/snapd/apparmor/profiles/snap*
browser_bin=/snap/chromium/current/usr/lib/chromium-browser/chrome
browser_video_switches='--use-gl=egl --gles --disable-gpu-compositing'
fi
fi

browser_if_proxy=''
ping -c1 -4 $NETWORK_TEST_INTERNET || ping -c1 -4 $NETWORK_TEST_INTERNET || {
    browser_if_proxy=" --proxy-server=\"http://$WHEN_NEEDED_HTTP_PROXY\" "
}

## chrome://gpu to check HW
# NOTE: extra vars in /etc/chromium-browser/customizations/00-rpi-vars
# --test-type               # removes warrnings about security
# --no-sandbox              # needed when running as root
# --disable-web-security    # allow local files and CORS
# --window-size=1920,1080   # size >= screen so start-fullscreen fills full area
# --check-for-update-interval=10 # 10 seconds show update box..(default is like ~7h)
# --simulate-critical-update     # shows update as an icon hidden from user when in fullscreen
# --enable-logging=stderr '--vmodule=network_*=3'  # log url requests
# TODO: perhaps improve performance....
# --disable-gpu-compositing (seems to lower performance)
# --use-gl=egl --gles --enable-features=VaapiVideoDecoder --enable-zero-copy --force-gpu-rasterization --enable-drdc --canvas-oop-rasterization --ignore-gpu-blocklist \
# --enable-accelerated-video-decode --ignore-gpu-blacklist
## TODO: should disables https security checks because CA might expire.
$browser_bin --test-type --no-sandbox --disable-web-security --disable-site-isolation-trials \
    --ignore-certificate-errors --user-data-dir=/tmp/kiosk \
    --window-position=0,0 --window-size=${VSCREEN_WIDTH},${VSCREEN_HEIGHT} --start-fullscreen \
    --check-for-update-interval=3628800 --simulate-critical-update \
    --disable-session-crashed-bubble --disable-restore-session-state \
    --noerrdialogs --disable-infobars --autoplay-policy=no-user-gesture-required \
    --enable-fast-unload --enable-tcp-fast-open --dns-prefetch-disable \
    $browser_video_switches $browser_if_proxy \
    --enable-logging=stderr '--vmodule=network_*=3' \
    --kiosk http://127.0.0.1/index.html
