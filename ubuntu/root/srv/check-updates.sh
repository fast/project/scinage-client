#!/bin/bash
set -u
sleep $((60 * 5 + RANDOM % 30))
while :
do
    source /config.sh
    # -- UPDATES
    # NOTE: skip ssl check on curl because the local CA chain could expire!
    # WARN: device can be owned if someone uses a DHCP server and hijacks DNS. I dont care.
    /get-fw-lock.sh
    curl -L --fail --insecure "$CHECK_UPDATES_ENDPOINT" | bash
    /set-fw-unlock.sh
    # recheck for updates after ~4hours
    sleep $((14400 + RANDOM % 250))
done
