#!/bin/bash
source /config.sh


# basicaly this just set the led to hartbeat.
/get-fw-lock.sh ; /set-fw-unlock.sh


export XAUTHORITY=/tmp/kiosk/.Xauthority
export XDG_RUNTIME_DIR=/tmp/kiosk
rm -rf "$XDG_RUNTIME_DIR" || true
mkdir "$XDG_RUNTIME_DIR" || true


function get_firstslide_control_data {
  slide=$(curl -s $SCREEN_URL | grep 'var link =' | cut -d '"' -f 2)
  slide_data=$(curl -s "$SITE_URL$slide")
  # also get first slide when slideshow mode
  slideshow_firstslide=$(echo "$slide_data" | grep 'nextframe' | tail -n 1 | cut -d "'" -f 2)
  slide_data="$slide_data and slideshow $(curl -s "$SITE_URL$firstslide")"
  echo "$slide_data" | sed -n '/BEGIN_PLAYLIST_VLC/,/END_PLAYLIST_VLC/p' > /tmp/playlist.vlc.latest
  echo "$slide_data" | sed -n '/BEGIN_CONFIG_SCREENS_CUSTOM/,/END_CONFIG_SCREENS_CUSTOM/p' > /tmp/config-screens-custom.sh.latest
}


function restart_kiosk_on_control_data_change {
  while true
  do
    # wait 10min before bugging scinage for updates
    recheck_sleep=600s
    # unless trying custom screen modes
    if test -s /tmp/config-screens-custom.sh
    then
      recheck_sleep=10s
    fi
    sleep $recheck_sleep

    get_firstslide_control_data
    if ! diff /tmp/config-screens-custom.sh.latest /tmp/config-screens-custom.sh
    then
      wall 'Change display config.'
      sleep 2
      systemctl restart kiosk
      exit 0
    fi
    if ! diff /tmp/playlist.vlc.latest /tmp/playlist.vlc
    then
      wall 'Change in VLC playlist.'
      sleep 5
      systemctl restart kiosk
      exit 0
    fi
  done
}


get_firstslide_control_data
cp /tmp/playlist.vlc.latest /tmp/playlist.vlc
cp /tmp/config-screens-custom.sh.latest /tmp/config-screens-custom.sh
restart_kiosk_on_control_data_change &


if test -s /tmp/config-screens-custom.sh
then
  mount -o remount,rw /media/root-ro || true
  cp /tmp/config-screens-custom.sh /media/root-ro/config-screens-custom.sh
  cp /tmp/config-screens-custom.sh /config-screens-custom.sh
  sync || true
  mount -o remount,ro /media/root-ro || true
  sleep 2
fi


if test -s /tmp/playlist.vlc
then
  startx /srv/kiosk-vlc.sh -- -nocursor
else
  startx /srv/kiosk-chromium.sh -- -nocursor
fi
