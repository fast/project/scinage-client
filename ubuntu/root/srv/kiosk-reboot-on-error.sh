#!/bin/bash
source /config.sh
#set -exu -o pipefail

while :
do
    # The min wait must be larger than 60sec to allow for check-updates to run
    sleep $((80 + RANDOM % 25))

    if ! test -e /sys/class/graphics/fb0/virtual_size ; then
        sleep 10
        if ! test -e /sys/class/graphics/fb0/virtual_size ; then
            wall '** No screen connected'
            /get-fw-lock.sh
            echo b > /proc/sysrq-trigger
        fi
    fi

    # NOTE: tvservice is a raspberry-pi tool
    #     if command -v tvservice ; then
    #         ## If there is a timeout when trying to access tvservice; something big crashed
    #         if ! timeout --signal 9 3 tvservice --list
    #         then
    #             wall '** Kernel driver fail'
    #             # hard reboot because normal reboot could lock up system
    #             /get-fw-lock.sh
    #             echo b > /proc/sysrq-trigger
    #         fi
    #         # todo: when using KMS use tool modetest to check screen attached
    #         if timeout --signal 9 4 tvservice --list | grep -q '0 attached'
    #         then
    #             wall '** HDMI seems to be unplugged or was offline. rebooting'
    #             sleep 10
    #             /get-fw-lock.sh
    #             reboot
    #         fi
    #     fi

    # Catch low memory .. perhaps a common web browser crash.
    percent_mem_used=$(printf "%.0f" $(free | grep Mem | awk '{print $3/$2 * 100}'))
    if test "$percent_mem_used" -gt "90"
    then
        wall "** Low memory. rebooting"
        sleep 10
        /get-fw-lock.sh
        reboot
    fi

    # no network, perhaps a reboot might fix...
    # also a reboot will show the network error screen
    if ! ping -4 -c 1 "$NETWORK_TEST_SERVER_1"
    then
        if ! ping -6 -c 1 "$NETWORK_TEST_SERVER_1"
        then
            ## enable dhcp on all interfaces
            ## this might help if a usb ethernet adaptor is plugged in
            dhcpcd || true
            # retry ping again perhaps service was just rebooting
            sleep 60
            if ! ping -c 1 "$NETWORK_TEST_SERVER_2"
            then
                # wait some time because we want to show the nice kiosk error webpage
                # and give the network some time to recover
                sleep 300
                if ! ping -c 1 "$NETWORK_TEST_SERVER_1"
                then
                    wall "** Network ping failing. rebooting"
                    sleep 10
                    /get-fw-lock.sh
                    reboot
                fi
            fi
        fi
    fi

    if systemctl is-active kiosk
    then

        # TODO: check health of connected screens
        #XAUTHORITY=/tmp/kiosk/.Xauthority XDG_RUNTIME_DIR=/tmp/kiosk xrandr -d :0 --listmonitors

        if pgrep kiosk-vlc
        then
            # TODO check if vlc in good health...
            true
        fi

        if pgrep kiosk-chromium
        then
            # WARN: i think i'll remove the web console log of "nextslide" it was a hack..
            # in place of that hack use a different hack :)
            # # The scinage webapp will log "nextslide" on each slide change.
            # # If we dont see a slide change, assume kiosk or scinage crashed.
            # if ! journalctl --unit kiosk --since "15 minute ago" --no-pager | grep -q 'nextslide'
            # then
            #     wall "** kiosk service has not reported nextslide. Rebooting"
            #     sleep 30
            #     # hard reboot because normal reboot has at times lock up system
            #     # the likly cause of no nextslide is a GPU drivercrash.
            #     /get-fw-lock.sh
            #     echo b > /proc/sysrq-trigger
            # fi

            # This is a hack.. have chrome set with log network '--vmodule=network_*=3'
            # and check it is hitting /ping/
            if ! journalctl --unit kiosk --since "20 minute ago" --no-pager | grep -q '/ping/'
            then
                # give kiosk some extra time to hit slideshow
                sleep 240
                if ! journalctl --unit kiosk --since "20 minute ago" --no-pager | grep -q '/ping/'
                then
                    wall "** kiosk service seems to be down. rebooting"
                    sleep 10
                    # hard reboot because normal reboot has at times hit a lock up
                    /get-fw-lock.sh
                    echo b > /proc/sysrq-trigger
                fi
            fi
        fi

    fi
done
