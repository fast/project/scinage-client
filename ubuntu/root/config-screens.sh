#!/bin/bash

# if you have multiple screens or rotated screens you can configure them here
# for example you could have 4 1080p screens and link them all up in a 4k grid
# or perhaps a screen rotated to be vertical

## get some hints from the linux kernel about the screen attached to the system
# cat /sys/devices/virtual/graphics/fbcon/subsystem/fb0/virtual_size
_video_driver_screen_width=$(cat /sys/class/graphics/fb0/virtual_size | cut -d ',' -f1)
_video_driver_screen_height=$(cat /sys/class/graphics/fb0/virtual_size | cut -d ',' -f2)

## Set virtual screen shape and size
## =================================

## The VSCREEN is the area of all screen(s)
## for example if you have a grid of 4 screens this is the full area!

# default is assumed a TV mounted normal (horizontal)
VSCREEN_WIDTH=$_video_driver_screen_width
VSCREEN_HEIGHT=$_video_driver_screen_height

## if screen is rotated vertical; flip values
# VSCREEN_WIDTH=$_video_driver_screen_width
# VSCREEN_HEIGHT=$_video_driver_screen_height

## 4k or 4 1080p screens linked in a grid
# VSCREEN_WIDTH=3840
# VSCREEN_HEIGHT=2160

## FORCE 1080p ** if issues
# VSCREEN_WIDTH=1920
# VSCREEN_HEIGHT=1080

# Configure screens and placment
# ==============================

# AUTO size
SCREEN1_WIDTH=$_video_driver_screen_width
SCREEN1_HEIGHT=$_video_driver_screen_height
# FORCE 4k
#SCREEN1_WIDTH=3840
#SCREEN1_HEIGHT=2160
# FORCE 1080p
#SCREEN1_WIDTH=1920
#SCREEN1_HEIGHT=1080

export XAUTHORITY=/tmp/kiosk/.Xauthority
export XDG_RUNTIME_DIR=/tmp/kiosk

#== debug
# list screen modes
xrandr -d :0 || true
# list screens
xrandr -d :0 --listmonitors || true

# if screen horizontal (normal)
xrandr -d :0 --orientation normal -s ${SCREEN1_WIDTH}x${SCREEN1_HEIGHT}

# if screen vertical rotated left or right
#xrandr -d :0 --orientation left -s ${SCREEN1_WIDTH}x${SCREEN1_HEIGHT}
#xrandr -d :0 --orientation right -s ${SCREEN1_WIDTH}x${SCREEN1_HEIGHT}

# if screen has overscan
#xrandr -d :0 --output HDMI1 --mode 3840x2160 --panning 3840x2160+1920+1080

# if a grid of 4 screen perhaps something like
#xrandr -d :0 --output HDMI1 --auto
#xrandr -d :0 --output HDMI2 --auto --right-of HDMI1
#xrandr -d :0 --output HDMI3 --auto --below HDMI1
#xrandr -d :0 --output HDMI4 --auto --right-of HDMI3 --below HDMI2

# also pull in custom config
# this file can be managed from scinage using a slide widget
source /config-screens-custom.sh || true
