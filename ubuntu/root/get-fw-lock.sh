#!/bin/bash
while test -e /var/lock/fwlock ; do sleep 10 ; done
while test -e /var/lock/fwlock ; do sleep 2 ; done
# WARN: there is a race condishion here
echo "PID.$PID PPID.$PPID BASHPID.$BASHPID PWD.$PWD BASH_SOURCE.$BASH_SOURCE" > /var/lock/fwlock
sleep 1
# if apt seems to be running keep looping until free
while fuser /var/lib/dpkg/lock >/dev/null 2>&1 ; do sleep 2 ; done

# update red LED to heartbeat
# the timeouts are needed if the device hit a critical issue talking to internal services.
# in the event of a ctitical issue the "reboot-on-kiosk-error should reboot.
timeout -s9 2 echo timer > /sys/class/leds/led0/trigger || true
timeout -s9 2 echo 1 > /sys/class/leds/led0/brightness || true
timeout -s9 2 echo timer > /sys/class/leds/led1/trigger || true
timeout -s9 2 echo 1 > /sys/class/leds/led1/brightness || true

