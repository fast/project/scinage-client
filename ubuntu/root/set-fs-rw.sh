#!/bin/bash
# if this system is perhaps running in read only mode. Set to read write mode on next reboot and also reboot

flagreboot=0
if test -e /etc/overlayroot.local.conf ; then
    mv /etc/overlayroot.local.conf /etc/overlayroot.local.conf_disable || true
    flagreboot=1
fi
if test -e /media/root-ro/etc/overlayroot.local.conf ; then
    mount -o remount,rw /media/root-ro || true
    mv /media/root-ro/etc/overlayroot.local.conf /media/root-ro/etc/overlayroot.local.conf_disable || true
    flagreboot=1
fi

if test "$flagreboot" = '1' ; then
    reboot
fi
