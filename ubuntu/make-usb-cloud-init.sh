#!/usr/bin/env bash
set -eu

# HINT: use 'dmesg' to find what your device is named.
lsblk | grep disk
echo "** ENTER USB DEVICE PATH (/dev/sdx)"
read usbdev

sudo date
#<<EOF sudo bash
sudo umount $usbdev || true
# format using the FAT32 (`-F 32`) format and name (`-n`) the volume 'CIDATA' (`-I` for ignoring safety checks)
sudo mkfs.vfat -I -F 32 -n 'CIDATA' $usbdev

rm -rf /tmp/cidata || true
mkdir /tmp/cidata
sudo mount $usbdev /tmp/cidata

sudo cp ./meta-data /tmp/cidata/meta-data
sudo cp ./user-data /tmp/cidata/user-data

sync
sudo umount $usbdev

rmdir /tmp/cidata
#EOF
