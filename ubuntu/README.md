Install directions
==================

The following install directions are tested with Ubuntu22.04, 23.04 and 24.04 Desktop and Server installs.

Using the Ubuntu-Server install is ideal because less junk is included but Ubuntu Desktop also works.


Hardware tested
---------------

Intel NUC: Most version supports 4k60hz but you might need to use DP and quality cables and adapters! That said 4k30hz is likly good enough.

  - Simply boot from a USB that has Ubuntu installer

The Intel Mac-mini(2014) supports a max video mode of 4k30hz. For more details on the hardware see: https://support.apple.com/kb/SP710?locale=en_GB

  - Hold "Left ALT" or "MacOption" key and select the USB boot device.


Making install USB key
----------------------

Write Ubuntu-Server install ISO to a USB key.
 - https://releases.ubuntu.com/24.04/
 - https://releases.ubuntu.com/22.04/ubuntu-22.04.2-live-server-amd64.iso

```
cd ~
wget https://releases.ubuntu.com/24.04/ubuntu-24.04.0-live-server-amd64.iso
# OR: ubuntu24.04
lsblk | grep disk
# replace /dev/sd? with your USB key device path.
sudo cp ./ubuntu-*-live-server-amd64.iso /dev/sd?
sync
```

Install
-------

Use the USB to install OS.

While installing (Ubuntu-Server)
  - (optional) select "minimized" version to reduce the footprint.
  - networking: DHCPv4
    - if doing an offline install still select DHCP as the config.
  - set admin user as: "ubuntu" or "oem"
    - user "ubuntu" and "oem" get locked by Scinage-client deploy script.
  - hostname: does not matter (scinage-client deploy script will auto generate)

After rebooted log in the new OS install. Run the Scinage-client deploy script.

```
sudo -i
apt update
apt install -y git
cd /root
git clone https://git.uwaterloo.ca/fast/project/scinage-client.git
cd scinage-client/ubuntu
<<EOF cat > /root/.ssh/authorized_keys2
# OPTIONAL personal ssh public keys to access the device
EOF
./deploy
```

The deployment script will copy files to the root filesystem and then issue a reboot command. On the next bootup the system will run script `/deploy-script`. The install process might take `~15minutes`. Once complete the system will do one final reboot into a read only filesystem and show some on screen setup directions.

Security notes:
 - OS boots as readonly filesystem
 - System is set to not send telemetry data.
 - Webbrowser(Chrome) set to ignore certs and security. This prevents breakage.

Managment:
 - Accepts SSH connections from root@main.fast.uwaterloo.ca so Scinage admins can aid
 - If you want SSH access add your keys to `/root/.ssh/authorized_keys2`
 - Critical OTA updates can be scripted by a Scinage admin from https://bringup.fast.uwaterloo.ca


Advanced - Screen configuration
-------------------------------

By default the screen is assumed to be mounted horizontal and there is only one conected to the device.

There is support for grids of screens and vertical mounting.

Before or after deployment you can tweak `config-screens.sh`
See [config-screens.sh](root/config-screens.sh)

Changes after deploying the software can be done using SSH to connect to root with a workflow like:

```
# set device to RW mode
ssh root@device /set-fs-rw.sh

ssh root@device
nano /config-screen.sh
reboot

ssh root@device /set-fs-ro.sh
```

Advanced - proxy config
-------------------------------

If needed you can set a proxy-server doing the following.

```
# set device to RW mode
ssh root@device /set-fs-rw.sh

ssh root@device
nano /srv/kiosk-chromium.sh
# update the browser to have soomething like
#    --proxy-server="http://calamar.private.uwaterloo.ca:3128" \
#    --kiosk http://127.0.0.1/index.html

reboot

ssh root@device /set-fs-ro.sh
```


Advanced - Automate deployment using cloud-init
-----------------------------------------------

If you are like me and want to make deploying 20 of these faster...

Create a second USB key with some cloud-init config.
 - Set disk label 'CIDATA'
 - create files 'meta-data' and 'user-data'

Canonical's installer Subiquity requires kernel cmdline 'autoinstall' or it forces question "Continue with autoinstall?"

HINT: during the install grub menu you can hit "e" and manualy add kernel argument "autoinstall" to the linux boot command line during each install. (not ideal but it works)

To make the second USB stick with the install automation deployment files:

RUN: `./make-usb-cloud-init.sh`


