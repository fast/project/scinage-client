See the readme in the sub folders for more details.

Tested Hardware:
 - [Hardware - Intel NUC](ubuntu/README.md)
 - [Hardware - Mac-mini(2014)](ubuntu/README.md)
 - Raspberry Pi 4 (old might have issues)


WIFI
====

To enable wifi on device you will need to ssh to it and update some files or include changes when installing device.

```
# perhaps needed for wifi on ubuntu server version
apt install -y wpasupplicant
```


vi /etc/netplan/xxxx?

```
network: #< might need another 'network:'
 version: 2
 ethernets:
   enp0s31f6:
     dhcp4: yes
 wifis:
     # protected WPA WPA2 ...
     wlp2s4:
       dhcp4: true
       optional: true
       access-points:
         University:
           password: mysecret
     # Open
     wlp2s0:
       dhcp4: true
       optional: true
       access-points:
         University: {}
     # mschapv2 https://bugs.launchpad.net/ubuntu/+source/wpa/+bug/1962541
     wlp1s0:
       dhcp4: true
       optional: true
       access-points:
         "<SSID>":
             auth:
               key-management: eap
               method: peap
               ca-certificate: /etc/WIFI.pem
               identity: <hiddenid>
               password: <secret>
```
