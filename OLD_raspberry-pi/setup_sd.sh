#!/bin/bash

echo "Run with root"
test $UID = 0 || exit 1
set -xeu


chroot=/mnt/pi


# mount pi image.
# https://downloads.raspberrypi.org/raspios_lite_arm64/images/
if ! test -e 2022-09-22-raspios-bullseye-arm64-lite.img
then
    #wget https://downloads.raspberrypi.org/raspios_lite_arm64/images/raspios_lite_arm64-2022-04-07/2022-04-07-raspios-bullseye-arm64-lite.zip
    #unzip 2022-04-07-raspios-bullseye-arm64-lite.zip
    wget https://downloads.raspberrypi.org/raspios_lite_arm64/images/raspios_lite_arm64-2022-09-26/2022-09-22-raspios-bullseye-arm64-lite.img.xz
    xz -d ./2022-09-22-raspios-bullseye-arm64-lite.img.xz
    #rm ./2022-09-22-raspios-bullseye-arm64-lite.img.xz
fi
rm -rf custom.img || true
cp 2022-09-22-raspios-bullseye-arm64-lite.img custom.img
rm -rf $chroot || true
mkdir $chroot || true
fdisk -l ./custom.img
## NOTE: 2017-xx-xx - blocksize and offsets blocksize:512, boot:8192, os:94208
## NOTE: 2018-03-13 - blocksize and offsets blocksize:512, boot:8192, os:98304
## NOTE: 2020-02-13 - blocksize and offsets blocksize:512, boot:8192, os:532480
## NOTE: 2022-01-18 - blocksize and offsets blocksize:512, boot:8192, os:532480
## NOTE: 2022-09-22 - blocksize and offsets blocksize:512, boot:8192, os:532480
blocksize=512
boot_sector=8192
os_sector=532480
offset=$(($os_sector * $blocksize))
mount -o loop,offset=$offset ./custom.img $chroot
## boot offset is like: 8192 * 512 = 4194304
offset=$(($boot_sector * $blocksize))
sizelimit=$((($os_sector - $boot_sector) * $blocksize))
mount -o loop,offset=$offset,sizelimit=$sizelimit ./custom.img $chroot/boot


cp -r -v ./root/* $chroot/
# remove this service that asks on first boot to create an user
rm $chroot/etc/systemd/system/multi-user.target.wants/userconfig.service || true


sync
umount "$chroot/boot"
umount "$chroot"

echo "# WRITE TO A SD CARD"
echo "# dd if=./custom.img of=/dev/sd?? bs=16M"
echo "sudo cp ./custom.img /dev/sd? ; sync"
