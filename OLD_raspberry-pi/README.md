First install
-------------

If you want to enable WIFI copy `root/boot/wpa_supplicant.conf_example` to `root/boot/wpa_supplicant.conf` and edit that file to include your configuration.

Run `sudo ./setup_sd.sh` to create `custom.img`.

You can then write `custom.img` onto a SD card for using on a Raspberry Pi 4.

For example.

```
sudo ./setup_sd.sh
# assuming your SD card device is sdcard1.
# HINT: use `sudo fdisk -l` to find your SD card device
sudo cp ./custom.img /dev/sdcard1
sync
```

After writing the image onto a SD card. Plug it into a PI and boot it up with internet connected.

The PI will install the base operating system and all the needed packages. This might take 1hour. Once complete it will reboot and should display the Scinage setup screen.


Bulk deployment
---------------

If you want to create a bunch of these devices you can take a shortcut.
Do the above steps above but use a smaller SD card lets say around 8GB.
Once that PI has all the packages and is showing the Scinage setup screen power it down.

You can then backup the SD card to a file and write it directly to other SD cards of the same size or larger.

for example

```
# backup the final SD card from a PI4 that is operational
cp /dev/sdcard ~/final.img
# replace the master SD card with a black in your computer
# the write the master final copy to the blank SD card
cp ~/final.img /dev/sdcard_blank
# ensure all writes completed
sync
```
