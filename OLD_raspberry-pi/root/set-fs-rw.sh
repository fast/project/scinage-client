#!/bin/bash
echo 'XXX Set main FS mounts as RW XXX'
mount / -o remount,rw
mount /boot -o remount,rw

# reoad so it can see the FS is now RW
systemctl restart systemd-logind.service
