#!/bin/bash
source /config.sh


# basicaly this just set the led to hartbeat.
/get-fw-lock.sh ; /set-fw-unlock.sh


export XAUTHORITY=/tmp/kiosk/.Xauthority
export XDG_RUNTIME_DIR=/tmp/kiosk
# stop kiosks
#pkill kiosk-vlc.sh || true
#pkill kiosk-chromium.sh || true
#sleep 5
rm -rf "$XDG_RUNTIME_DIR" || true
mkdir "$XDG_RUNTIME_DIR" || true


function get_playlist_latest_vlc {
    echo '' > /tmp/playlist-latest.vlc
    slideshow=$(curl -s $SCREEN_URL | grep 'var link =' | cut -d '"' -f 2)
    firstslide=$SITE_URL$(curl -s "$SITE_URL$slideshow" | grep 'nextframe' | tail -n 1 | cut -d "'" -f 2)
    curl -s "$firstslide" | sed -n '/BEGIN_PLAYLIST_VLC/,/END_PLAYLIST_VLC/p' > /tmp/playlist-latest.vlc
    # dos2unix /tmp/playlist-latest.vlc
}

function restart_kiosk_on_vlc_playlist_change {
    while true
    do
        # wait 10min before bugging scinage for playlist updates
        sleep 600
        get_playlist_latest_vlc
        if ! diff /tmp/playlist-latest.vlc /tmp/playlist.vlc
        then
        wall 'Change in VLC playlist.'
        sleep 5
        systemctl restart kiosk
        exit 0
        fi
    done
}

get_playlist_latest_vlc
cp /tmp/playlist-latest.vlc /tmp/playlist.vlc
restart_kiosk_on_vlc_playlist_change &

if test -s /tmp/playlist.vlc
then
  startx /srv/kiosk-vlc.sh -- -nocursor
else
  startx /srv/kiosk-chromium.sh -- -nocursor
fi
