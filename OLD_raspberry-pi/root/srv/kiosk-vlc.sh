#!/bin/bash
xset s noblank
xset s off
xset -dpms
source /config.sh

xrandr -d :0 -s ${SCREEN_WIDTH}x${SCREEN_HEIGHT}

# xsetroot -solid gray
#
# apt install -y vlc
# # a hack to run vlc as root
# cp /usr/bin/vlc /usr/bin/vlc-root
# # hack to force VLC's userid lookup to check for root zero to a larger number
# sed -i 's/geteuid/getppid/' /usr/bin/vlc-root

# get the slideshow for the screen
#slideshow=$(curl -s $SCREEN_URL | grep 'var link =' | cut -d '"' -f 2)
#firstslide=$SITE_URL$(curl -s "$SITE_URL$slideshow" | grep 'nextframe' | tail -n 1 | cut -d "'" -f 2)
#curl -s "$firstslide" | sed -n '/BEGIN_PLAYLIST_VLC/,/END_PLAYLIST_VLC/p' > /tmp/playlist.vlc

function enforce_full_screen {
  while true
  do
  sleep 1
  DISPLAY=:0 xdotool search --name "VLC media player" windowsize ${SCREEN_WIDTH} ${SCREEN_HEIGHT}
  done
}
enforce_full_screen &
# --fullscreen #WARN: this has some strange issues, looks like buffers get out of order.
# in place of fullscreen we'll need to do a fack fullscreen and resize the X window using xdotool
# --verbose 0-3
# --play-and-exit
# --no-video-deco
# --no-osd .. disable file name playing
vlc-root --ignore-config --no-osd --intf dummy --video-on-top  --no-qt-privacy-ask \
  --loop /tmp/playlist.vlc

# DISPLAY=:0 xwininfo -root -tree
# xwininfo -name "VLC media player"
#DISPLAY=:0 xprop -name "VLC media player"
# wmctrl -r "VLC media player" -e 10,0,0,${SCREEN_WIDTH}x${SCREEN_HEIGHT}
#wmctrl -r vlc-root -e -e 0,0,0,${SCREEN_WIDTH}x${SCREEN_HEIGHT}
fg
# --width ${SCREEN_WIDTH} --height ${SCREEN_HEIGHT}
# mmal_xsplitter vout display error: Failed to open Xsplitter:mmal_vout module

#
# ## ** /slides/view/3978/
# # get the nextslide meta data url
# ## NOTE: cant use nextslide because we need first slide.
# nextslide_meta=$(echo "$slideshow" | sed 's|/view/|/nextslide/|')0/
# nextslide_url=$SITE_URL$(curl -s $SITE_URL${nextslide_meta} | cut -d '"' -f 4)
# echo $nextslide_url
# # TODO: look for video mode and playlist.
# curl -s $nextslide_url
# # save playlist to /tmp/playlist
# # change to VLC mode.
# https://scinage.uwaterloo.ca/slideshows/view/371/
