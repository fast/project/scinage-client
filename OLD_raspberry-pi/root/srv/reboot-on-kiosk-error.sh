#!/bin/bash
source /config.sh
#set -exu -o pipefail

while :
do
    sleep $((60 + RANDOM % 25))

    ## If there is a timeout when trying to access tvservice; something big crashed
    if ! timeout --signal 9 3 tvservice --list
    then
        wall '** Kernel driver fail'
        # hard reboot because normal reboot could lock up system
        /get-fw-lock.sh
        echo b > /proc/sysrq-trigger
    fi

    # todo: when using KMS use tool modetest to check screen attached
    if timeout --signal 9 4 tvservice --list | grep -q '0 attached'
    then
        wall '** HDMI seems to be unplugged or was offline. rebooting'
        sleep 10
        /get-fw-lock.sh
        reboot
    fi

    # Catch low memory .. perhaps a common web browser crash.
    percent_mem_used=$(printf "%.0f" $(free | grep Mem | awk '{print $3/$2 * 100}'))
    if test "$percent_mem_used" -gt "90"
    then
        wall "** Low memory. rebooting"
        sleep 10
        /get-fw-lock.sh
        reboot
    fi

    # no network, perhaps a reboot might fix...
    # also a reboot will show the network error screen
    if ! ping -c 1 "$NETWORK_TEST_SERVER_1"
    then
        # retry ping again perhaps service was just rebooting
        sleep 60
        if ! ping -c 1 "$NETWORK_TEST_SERVER_2"
        then
            # wait some time because we want to show the nice kiosk error webpage
            # and give the network some time to recover
            sleep 300
            if ! ping -c 1 "$NETWORK_TEST_SERVER_1"
            then
                wall "** Network ping failing. rebooting"
                sleep 10
                /get-fw-lock.sh
                reboot
            fi
        fi
    fi

    if systemctl is-active kiosk
    then

        if pgrep kiosk-vlc
        then
            # TODO check...
            true
        fi

        if pgrep kiosk-chromium
        then
            # WARN: i think i'll remove the web console log of "nextslide" it was a hack..
            # in place of that hack use a different hack :)
            # # The scinage webapp will log "nextslide" on each slide change.
            # # If we dont see a slide change, assume kiosk or scinage crashed.
            # if ! journalctl --unit kiosk --since "15 minute ago" --no-pager | grep -q 'nextslide'
            # then
            #     wall "** kiosk service has not reported nextslide. Rebooting"
            #     sleep 30
            #     # hard reboot because normal reboot has at times lock up system
            #     # the likly cause of no nextslide is a GPU drivercrash.
            #     /get-fw-lock.sh
            #     echo b > /proc/sysrq-trigger
            # fi
            if ! journalctl --unit kiosk --since "20 minute ago" --no-pager | grep -q '/slideshows/'
            then
                # give kiosk some extra time to get to hit slideshow
                sleep 120
                if ! journalctl --unit kiosk --since "20 minute ago" --no-pager | grep -q '/slideshows/'
                then
                    wall "** kiosk service seems to be down. rebooting"
                    sleep 10
                    # hard reboot because normal reboot has at times hit a lock up
                    /get-fw-lock.sh
                    echo b > /proc/sysrq-trigger
                fi
            fi
        fi

    fi
    # https://scinage.uwaterloo.ca/slides/view/3968/
    # https://scinage.uwaterloo.ca/slideshows/nextslide/371/0/
    # This is a hack.. Set chrome to log network '--vmodule=network_*=3'
    # so it prints out the URL requests
    # if we dont see any logs from kiosk hitting /nextframe reboot
    #     if journalctl --unit kiosk --since -12000s --no-pager | grep -q '/nextslide/'
    #     then
    #         /get-fw-lock.sh
    #         wall "** kiosk service seems to be down. rebooting"
    #         sleep 30
    #         reboot
    #     fi

done
