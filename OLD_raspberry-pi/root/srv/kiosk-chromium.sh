#!/bin/bash
xset s noblank
xset s off
xset -dpms
source /config.sh

xrandr -d :0 -s ${SCREEN_WIDTH}x${SCREEN_HEIGHT}


## --brightness
## white flash when starting chrome in full screen.
## using these dont work ... --cast-app-background-color=223311aa --default-background-color=000000ff \
## can perhaps use dark mode but that changes theaming for webpages,
# WORKAROUND: ... nope cant do this because the PI doues not support this with FKMS or KMS driver
#xrandr -d :0 --output HDMI-1 --brightness 25



## chrome://gpu to check HW
# NOTE: extra vars in /etc/chromium-browser/customizations/00-rpi-vars
# --test-type               # removes warrnings about security
# --no-sandbox              # needed when running as root
# --disable-web-security    # allow local files and CORS
# --window-size=1920,1080   # size >= screen so start-fullscreen fills full area
# --check-for-update-interval=10 # 10 seconds show update box..(default is like ~7h)
# --simulate-critical-update     # shows update as an icon hidden from user when in fullscreen
# --enable-logging=stderr '--vmodule=network_*=3'  # log url requests
# TODO: perhaps improve performance....
# --disable-gpu-compositing (seems to lower performance)
# --use-gl=egl --gles --enable-features=VaapiVideoDecoder --enable-zero-copy --force-gpu-rasterization --enable-drdc --canvas-oop-rasterization --ignore-gpu-blocklist \
# --enable-accelerated-video-decode --ignore-gpu-blacklist
## TODO: should disables https security checks because CA might expire.
chromium-browser --test-type --no-sandbox --disable-web-security --user-data-dir=/tmp/kiosk \
    --window-position=0,0 --window-size=${SCREEN_WIDTH},${SCREEN_HEIGHT} --start-fullscreen \
    --check-for-update-interval=3628800 --simulate-critical-update \
    --disable-session-crashed-bubble --disable-restore-session-state \
    --noerrdialogs --disable-infobars --autoplay-policy=no-user-gesture-required \
    --enable-fast-unload --enable-tcp-fast-open --dns-prefetch-disable \
    --enable-zero-copy --force-gpu-rasterization --canvas-oop-rasterization \
    --enable-logging=stderr '--vmodule=network_*=3' \
    --kiosk http://127.0.0.1/index.html
