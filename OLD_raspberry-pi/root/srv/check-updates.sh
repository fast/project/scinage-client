#!/bin/bash
set -u
sleep $((60 + RANDOM % 10))
while :
do
    source /config.sh
    # -- UPDATES
    # NOTE: skip ssl check on curl because the local CA chain could expire!
    # WARN: device can be owned if someone uses a DHCP server and hijacks DNS. I dont care.
    /get-fw-lock.sh
    curl -L --fail --insecure "$CHECK_UPDATES_ENDPOINT" | bash
    /set-fw-unlock.sh
    sleep $((1200 + RANDOM % 50))
done
