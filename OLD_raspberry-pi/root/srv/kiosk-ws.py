#!/usr/bin/python3
import http.server
import socketserver
import threading
import urllib
import requests
import subprocess
import logging
import os

logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
logger.setLevel(os.environ.get('DEBUG_LEVEL', 'DEBUG'))

SCREEN_ID = os.environ.get('SCREEN_ID')
SITE_URL = os.environ.get('SITE_URL')
SCREEN_URL = os.environ.get('SCREEN_URL')
SCREEN_SETUP_URL = os.environ.get('SCREEN_SETUP_URL')
WS_HTML_TPL = os.environ.get('WS_HTML_TPL', '/srv/kiosk-ws.html')
WS_DEBUG_TIMEOUT = os.environ.get('WS_DEBUG_TIMEOUT', '5.5')
WS_REFRESH_RATE = os.environ.get('WS_REFRESH_RATE', '20')


def get_debug():
    date = subprocess.run("date --iso-8601=seconds", shell=True, capture_output=True) \
        .stdout.decode().replace('\n', '')
    version = subprocess.run("cat /version", shell=True, capture_output=True) \
        .stdout.decode().replace('\n', '')
    network = subprocess.run("ip a", shell=True, capture_output=True) \
        .stdout.decode()
    info = f'ADVANCED DETAILS\n\nscreen: {SCREEN_ID}\ndate: {date}\nversion: {version} \n\nnetworks:\n{network}'
    return info

class myHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        url_parse = urllib.parse.urlparse(self.path)
        query = urllib.parse.parse_qs(url_parse.query)
        if url_parse.path.startswith('/debug'):
            self.send_response(200)
            self.end_headers()
            self.wfile.write(get_debug().encode())
            return
        if url_parse.path.startswith('/check'):
            # proxy this call as a CORS workaround
            try:
                r = requests.get(SCREEN_URL, timeout=8.0)
                self.send_response(r.status_code)
            except Exception as ex:
                self.send_response(409)
            self.end_headers()
            return
        if url_parse.path.startswith('/index.html'):
            self.send_response(200)
            self.end_headers()
            pagetext = open(WS_HTML_TPL, 'r').read()
            pagetext = pagetext.replace('{{SCREEN_ID}}', SCREEN_ID)
            pagetext = pagetext.replace('{{SCREEN_URL}}', SCREEN_URL)
            pagetext = pagetext.replace('{{SCREEN_SETUP_URL}}', SCREEN_SETUP_URL)
            pagetext = pagetext.replace('{{SITE_URL}}', SITE_URL)
            pagetext = pagetext.replace('{{WS_DEBUG_TIMEOUT}}', WS_DEBUG_TIMEOUT)
            pagetext = pagetext.replace('{{WS_REFRESH_RATE}}', WS_REFRESH_RATE)
            self.wfile.write(pagetext.encode())
            return
        self.send_response(404)
        self.end_headers()

class ThreadedHTTPServer(socketserver.ThreadingMixIn, http.server.HTTPServer):
    """Handle requests in separate thread."""
if __name__ == "__main__":
    httpd = ThreadedHTTPServer(("127.0.0.1", 80), myHandler)
    logger.info('serving at localhost port {0}'.format(80))
    httpd.serve_forever()

