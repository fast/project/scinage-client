#!/bin/bash
xset s noblank
xset s off
xset -dpms
source /config.sh
xrandr -d :0 -s ${SCREEN_WIDTH}x${SCREEN_HEIGHT}

# apt install -t firefox-esr
## runs slow for video.
firefox -width 1920 -height 1080 \
    -kiosk http://127.0.0.1/index.html

# layers.acceleration.force-enabled     true
# media.libavcodec.allow-obsolete       true
# webgl.enable-webgl2                   false
#apt install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev sudo apt install libxvidcore-dev libx264-dev
