#!/bin/bash
xset s noblank
xset s off
xset -dpms
source /config.sh

xrandr -d :0 -s ${SCREEN_WIDTH}x${SCREEN_HEIGHT}

## kinda hacky
/get-fw-lock.sh
if ! command sway
then
/set-fs-rw.sh
apt install -y rpi-wayland sway
fi
/set-fw-unlock.sh


## TODO: this is a WiP.
## this will start sway as user pi with a needed login shell on seat0
# found wayland not to be faster than xord so gave up on this
exit 0

chvt 7
systemd-run \
    -p User=root \
    -p UtmpMode=user \
    -p TTYPath=/dev/tty7 \
    -p TTYReset=yes \
    -p StandardInput=tty \
    -p StandardOutput=tty \
    /sbin/agetty --autologin pi --noclear --login-program /bin/su --login-options '-l pi -c sway' tty7 vt100
