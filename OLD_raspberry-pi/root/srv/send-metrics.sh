#!/bin/bash
# DISABLE: bash strict checks to prevent some bad varables from preventing bringup updates
set -u
sleep $((10 + RANDOM % 10))
while :
do
    source /config.sh
    # -- METRIC
    primary_if="$(ip -oneline -4 route show to default | grep -o 'dev .*' | head -n1 | awk '{print $2}')"
    primary_ip="$(ip -oneline -4 addr show dev $primary_if | head -n1 | awk '{print $4}' | cut -d/ -f1)"
    primary_mac=$(ip -oneline -0 addr show dev $primary_if | head -n1 | grep -o -G 'link/ether.*' | awk '{print $2}')
    version=$(cat /version)
    uptime_seconds=$(cat /proc/uptime | awk '{print $1}')
    uptime_since="$(uptime -s)"
    percent_mem_used=$(printf "%.0f" $(free | grep Mem | awk '{print $3/$2 * 100}'))
    percent_disk_used=$(df -h | grep /dev/root | awk '{print $5}' | tr -d '%')
    percent_cpu_1m=$(cat /proc/loadavg | awk '{print $1}')
    percent_cpu_5m=$(cat /proc/loadavg | awk '{print $2}')
    percent_cpu_15m=$(cat /proc/loadavg | awk '{print $3}')
    cpu_tempature_c=$(($(cat /sys/class/thermal/thermal_zone0/temp) / 1000))
    curl -L --fail --insecure \
        --data "version=$version&primary_if=$primary_if&primary_ip=$primary_ip&primary_mac=$primary_mac&uptime_seconds=$uptime_seconds&uptime_since=$uptime_since&model=$MODEL_ID&percent_mem_used=$percent_mem_used&percent_disk_used=$percent_disk_used&percent_cpu_1m=$percent_cpu_1m&percent_cpu_5m=$percent_cpu_5m&percent_cpu_15m=$percent_cpu_15m&cpu_tempature_c=$cpu_tempature_c" \
        "$SEND_METRICS_ENDPOINT" || true
    sleep $((1200 + RANDOM % 50))
done
