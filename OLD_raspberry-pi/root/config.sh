#!/bin/bash
# programs source this file for env vars.
SERVER='scinage.uwaterloo.ca'
SITE_URL="https://${SERVER}"
DEVICE_ID=$(cat /proc/cpuinfo | grep Serial | cut -d ' ' -f2 | tr -d '\n')
MODEL_ID=$(cat /proc/cpuinfo | grep 'Model.*:' | head -n1 | cut -d ':' -f2 | tr -d ' ')
#MODEL_ID=$(cat /proc/cpuinfo | grep 'model name.*:' | head -n1 | cut -d ':' -f2 | tr -d ' ')
SCREEN_ID="sw-$(echo $DEVICE_ID | sha256sum | cut -c1-7)"
# (use hdmi port nearest the usb-c port if using 4k)
# 4K 3840x2160 - see: https://www.raspberrypi.org/documentation/configuration/hdmi-config.md
SCREEN_WIDTH=1920
SCREEN_HEIGHT=1080
SCREEN_URL="${SITE_URL}/screens/view/${SCREEN_ID}/"
SCREEN_SETUP_URL="${SITE_URL}/screens/new/${SCREEN_ID}/"
WS_REFRESH_RATE=10
WS_DEBUG_TIMEOUT=4
WS_HTML_TPL=/srv/kiosk-ws.html
#SET_RAMSWAP=0
ONETIME_SET_RO=0
NETWORK_TEST_SERVER_1=scinage.uwaterloo.ca
NETWORK_TEST_SERVER_2=uwaterloo.ca
TIMEZONE=America/Toronto

CHECK_UPDATES_ENDPOINT=https://bringup.fast.uwaterloo.ca/org/scinage/dev/$SCREEN_ID/ep/scinage
SEND_METRICS_ENDPOINT=https://bringup.fast.uwaterloo.ca/org/scinage/dev/$SCREEN_ID/set/$(date -I)
